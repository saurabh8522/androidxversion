package com.example.upsc.common;

public class Config {

    /*   --------------------    variable name for the signup      ------------------------*/
    public static final String name = "name";
    public static final String email = "email";
    public static final String password = "password";
    public static final  String mobile = "mobile";
    public static final String address = "address";
    public static final String channel = "channel";

    /* -----------------      URL to connnect to databases          --------------------------*/

    public static final String URL_Normal_Signup_Student = "https://upscnitk.000webhostapp.com/student_signup.php";
    public static final String URL_Feed_student = "https://upscnitk.000webhostapp.com/feed_student.php";
    public static final  String URL_save_post = "https://upscnitk.000webhostapp.com/save_post_student.php";
    public static final String DEVELOPER_KEY = "AIzaSyBEGoRdMTxcIG86La4Cdu29kj4B7PcX-V0";
    public static final String  URL_Get_Content = "https://upscnitk.000webhostapp.com/student_get_saved.php";
    public static String  URL_Change= "";

    public static final String URL_quiz_student = "https://upscnitk.000webhostapp.com/student_quiz.php";
    public static final String URL_ques_student="https://upscnitk.000webhostapp.com/question_list.php";
    public static final String URL_ques_upload="https://upscnitk.000webhostapp.com/upload_question.php";

    public static final String ques_title = "ques_title";
    public static final String option1name = "option1";
    public static final String option2name = "option2";
    public static final  String option3name = "option3";
    public static final String option4name = "option4";
    public static final String ques_answer = "ques_solution";
    public static final String shortAnswer = "short_answer";
    public static final String ques_no = "ques_no";
    public static final String quizname = "quiz_title";
    public static final String quiz_url = "quiz_url";
    public static final String ques_type = "ques_type";



}
