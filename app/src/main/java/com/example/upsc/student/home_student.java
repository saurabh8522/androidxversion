package com.example.upsc.student;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.upsc.R;
import com.example.upsc.common.Config;
import com.example.upsc.student.feed.feed;
import com.example.upsc.student.Motivation.MotivationFragment;


public class home_student extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Fragment fragment;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CardView feed_cardview,motivation_cardview,content_cardview;

    private OnFragmentInteractionListener mListener;

    public home_student() {
        // Required empty public constructor
    }


    public static home_student newInstance(String param1, String param2) {
        home_student fragment = new home_student();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_home_student, container, false);
        feed_cardview = (CardView)view.findViewById(R.id.feed_student);
        motivation_cardview = (CardView) view.findViewById(R.id.motivation_student);
        content_cardview = (CardView)view.findViewById(R.id.student_content);
        feed_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.URL_Change = Config.URL_Feed_student;
                fragment  = new feed();
                loadwindow(fragment);

            }
        });
        motivation_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new MotivationFragment();
                loadwindow(fragment);
            }
        });
        content_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.URL_Change  = Config.URL_Get_Content;
                fragment = new feed();
                loadwindow(fragment);
            }
        });
        return  view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void loadwindow(Fragment fragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}



