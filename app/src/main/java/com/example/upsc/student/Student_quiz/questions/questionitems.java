package com.example.upsc.student.Student_quiz.questions;

public class questionitems {
    public String ques_id,ques_no,ques_title,option1,option2,option3,option4,ans,solution,short_ans,question_type;

    public questionitems() {
    }

    public questionitems(String ques_id, String ques_no, String ques_title,
                         String option1, String option2, String option3, String option4, String ans,
                         String solution, String short_ans, String question_type) {
        this.ques_id = ques_id;
        this.ques_no = ques_no;
        this.ques_title = ques_title;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.ans = ans;
        this.short_ans = short_ans;
        this.solution = solution;
        this.question_type = question_type;

    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getQues_id() {
        return ques_id;
    }

    public void setQues_id(String ques_id) {
        this.ques_id = ques_id;
    }

    public String getQues_no() {
        return ques_no;
    }

    public void setQues_no(String ques_no) {
        this.ques_no = ques_no;
    }

    public String getQues_title() {
        return ques_title;
    }

    public void setQues_title(String ques_title) {
        this.ques_title = ques_title;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getShort_ans() {
        return short_ans;
    }

    public void setShort_ans(String short_ans) {
        this.short_ans = short_ans;
    }
}
