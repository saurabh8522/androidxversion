package com.example.upsc.student.Student_quiz.quiz_menu;

public class quizItems {
    private String quiz_name,quiz_image;



    public quizItems(String quiz_name, String quiz_image) {
        this.quiz_name = quiz_name;
        this.quiz_image = quiz_image;
    }

    public quizItems() {
    }

    public String getQuiz_name() {
        return quiz_name;
    }

    public void setQuiz_name(String quiz_name) {
        this.quiz_name = quiz_name;
    }
    public String getQuiz_image() {
        return quiz_image;
    }

    public void setQuiz_image(String quiz_image) {
        this.quiz_image = quiz_image;
    }


}
