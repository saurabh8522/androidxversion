package com.example.upsc;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.support.design.widget.BottomNavigationView;

import com.example.upsc.Volunteer.MyChannel.fragment_channel;
import com.example.upsc.Volunteer.Quiz.makingQuiz;
import com.example.upsc.common.SignUp;
import com.example.upsc.common.home;
import com.example.upsc.common.login;
import com.example.upsc.student.*;
import com.example.upsc.Volunteer.*;
import com.example.upsc.student.Student_quiz.questions.questions;
import com.example.upsc.student.Student_quiz.quiz_menu.student_quiz_list;
import com.example.upsc.student.feed.feed;

public class MainActivity extends AppCompatActivity implements blank.OnFragmentInteractionListener,Profile.OnFragmentInteractionListener, login.OnFragmentInteractionListener, home.OnFragmentInteractionListener
, SignUp.OnFragmentInteractionListener,home_student.OnFragmentInteractionListener,home_volunteer.OnFragmentInteractionListener,
feed.OnFragmentInteractionListener,student_quiz_list.OnFragmentInteractionListener,questions.OnFragmentInteractionListener,makingQuiz.OnFragmentInteractionListener,fragment_channel.OnFragmentInteractionListener{



    //Reclone here

    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // when app starts the splash fragment will open

        BottomNavigationView navigation = (BottomNavigationView)findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }
    // comment added by saurabh

    private void loadwindow(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case    R.id.dashboard:
                    fragment = new home_student();
                    loadwindow(fragment);

                    return true;

                case    R.id.profile:
                    fragment = new fragment_channel();
                    loadwindow(fragment);

                    return true;



            }
            return false;
        }
    };
    public void onFragmentInteraction(Uri uri) {

    }
}
