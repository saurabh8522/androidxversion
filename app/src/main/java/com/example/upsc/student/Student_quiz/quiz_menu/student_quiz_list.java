package com.example.upsc.student.Student_quiz.quiz_menu;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.example.upsc.R;
import com.example.upsc.common.AppController;
import com.example.upsc.common.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


public class student_quiz_list extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private  ListView listView_quiz;
    private List<com.example.upsc.student.Student_quiz.quiz_menu.quizItems>quizItems;
    private com.example.upsc.student.Student_quiz.quiz_menu.quizadapter quizadapter;


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public student_quiz_list() {

    }

    public static student_quiz_list newInstance(String param1, String param2) {
        student_quiz_list fragment = new student_quiz_list();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_student_quiz_list, container, false);
        listView_quiz = (ListView)view.findViewById(R.id.quiz_list);
        quizItems = new ArrayList<quizItems>();
        quizadapter = new quizadapter(getActivity(),quizItems,getContext());
        listView_quiz.setAdapter(quizadapter);

        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(Config.URL_quiz_student);
        if(entry != null){
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");

                parseJsonFeed(data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }
        else {
            StringRequest jsonReq = new StringRequest(Request.Method.GET,
                    Config.URL_quiz_student,  new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }









        return  view;
    }


    private void parseJsonFeed(String response) {
        try {
            //  Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();
            JSONArray quizArray = new JSONArray(response);

            for (int i = 0; i < quizArray.length(); i++) {
                JSONObject feedObj = (JSONObject) quizArray.get(i);


                quizItems item= new quizItems();

                item.setQuiz_name(feedObj.getString("quiz_title"));




                item.setQuiz_image(feedObj.getString("quiz_url"));




                quizItems.add(item);
            }

            // notify data changes to list adapater
            quizadapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
