package com.example.upsc.Volunteer.Quiz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.upsc.R;
import com.example.upsc.Volunteer.home_volunteer;
import com.example.upsc.common.AESCrypt;
import com.example.upsc.common.Config;

import java.util.HashMap;
import java.util.Map;

public class makingQuiz extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    EditText questionName,option1Name,option2Name,option3Name,option4Name,shortAnswer,answer;
    String questionname,option1name,option2name,option3name,option4name,shortanswer,Answer;
    String quesType="";
    String ques_no= "0";
    TextView quizName;
    RadioGroup radioGroup;
    RadioButton radioButton;
    Button submit,add;
    String m_text= "This is quiz";
    LinearLayout optionlayout,optionlayout2;
    EditText input;
    private String mParam1;
    int flag=0;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public makingQuiz() {

    }

    public static makingQuiz newInstance(String param1, String param2) {
        makingQuiz fragment = new makingQuiz();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_making_quiz, container, false);
        if(flag==0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Enter Quiz Name");
            input = new EditText(getActivity());
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    m_text = input.getText().toString();
                    quizName.setText(m_text);
                    Toast.makeText(getActivity(), m_text, Toast.LENGTH_SHORT).show();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
            flag=1;
        }
        quizName = (TextView) v.findViewById(R.id.QuizName);
        questionName = (EditText) v.findViewById(R.id.enterQuestion);
        option1Name = (EditText) v.findViewById(R.id.option1);
        option2Name = (EditText) v.findViewById(R.id.option2);
        option3Name = (EditText) v.findViewById(R.id.option3);
        option4Name = (EditText) v.findViewById(R.id.option4);
        answer = (EditText) v.findViewById(R.id.answer);
        shortAnswer = (EditText) v.findViewById(R.id.shortanswer);

        submit = (Button) v.findViewById(R.id.btnsubmit);
        add=(Button) v.findViewById(R.id.btnadd);
        radioGroup=(RadioGroup) v.findViewById(R.id.radio);
        radioButton=(RadioButton)v.findViewById(radioGroup.getCheckedRadioButtonId());

        optionlayout = (LinearLayout) v.findViewById(R.id.options_layout);
        optionlayout2 = (LinearLayout) v.findViewById(R.id.ques_layout);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioButton1:
                        shortAnswer.setVisibility(View.VISIBLE);
                        optionlayout.setVisibility(View.INVISIBLE);
                        quesType="Type2";
                        break;
                    case R.id.radioButton2:
                        shortAnswer.setVisibility(View.INVISIBLE);
                        optionlayout.setVisibility(View.VISIBLE);
                        quesType="Type1";
                        // do operations specific to this selection
                        break;

                        // do operations specific to this selection

                }
            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //addquiz();
                Toast.makeText(getActivity(),"quiz added successfully",Toast.LENGTH_SHORT).show();

            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Question: "+questionName.getText().toString()+"\n"+"Answer:"+shortAnswer.getText().toString()+
                        answer.getText().toString()+option1Name.getText().toString()+"\n"
                        +option2Name.getText().toString()+"\n"+option3Name.getText().toString()+"\n"+option4Name.getText().toString()+"\n");

                builder.setMessage("Are you sure ?");

                // add the buttons
                builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addquestion();
                        Toast.makeText(getActivity(),"question added successfully",Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("");
////eeeeeeee
                        builder.setMessage("Are you sure ?");

                        // add the buttons
                        builder.setPositiveButton("Add Question", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // do something like...
                                //launchMissile();
                                Toast.makeText(getActivity(),"question added successfully",Toast.LENGTH_SHORT).show();
                                if(flag==0) {
                                    optionlayout2.setVisibility(View.VISIBLE);
                                    optionlayout.setVisibility(View.INVISIBLE);
                                    Fragment fragment = new makingQuiz();
                                    loadwindow(fragment);

                                }
                                else
                                {
                                    optionlayout2.setVisibility(View.VISIBLE);
                                    optionlayout.setVisibility(View.INVISIBLE);
                                }


                            }

                        });
                        builder.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        // create and show the alert dialog
                        AlertDialog dialog1 = builder.create();
                        dialog1.show();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        return v;
    }
    private void loadwindow(Fragment fragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
    public void addquestion(){
        final String questionname = questionName.getText().toString().trim();
        final String  option1name = option1Name.getText().toString();
        final String option2name = option2Name.getText().toString().trim();
        final String  option3name = option3Name.getText().toString();
        final String option4name = option4Name.getText().toString().trim();
        final String shortanswer = shortAnswer.getText().toString().trim();
        final String Answer = answer.getText().toString().trim();
        final String quizname = m_text;
        final String quizurl = " ";
        final String typeq = quesType;

        try {
            //final_pass  = AESCrypt.encrypt(string_pass);
        }
        catch (Exception e){

        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.URL_ques_upload,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //  SharedPrefsMethods.saveprofiledata("Logined",email,mobile,spinner,name);


                        Toast.makeText(getActivity(),ServerResponse,Toast.LENGTH_LONG).show();
                        clear();

                        // Hiding the progress dialog after all task complete.





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.


                        // Showing error message if something goes wrong.
                        Toast.makeText(getContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                // The first argument should be same sa your MySQL database table columns.
                params.put(Config.ques_title,questionname );
                params.put(Config.option1name,option1name);
                params.put(Config.option2name,option2name);
                params.put(Config.option3name,option3name);
                params.put(Config.option4name,option4name);
                params.put(Config.ques_answer,Answer);
                params.put(Config.shortAnswer,shortanswer);
                params.put(Config.ques_no,ques_no);
                params.put(Config.quizname,quizname);
                params.put(Config.quiz_url,quizurl);
                params.put(Config.ques_type,typeq);
               // Log.i("question type: ",typeq);
                //  params.put("User_Password", PasswordHolder);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);


    }
    public void clear(){
        questionName.setText("");
        option1Name.setText("");
        option2Name.setText("");
        option3Name.setText("");
        option4Name.setText("");
        answer.setText("");
        shortAnswer.setText("");
    }
}
