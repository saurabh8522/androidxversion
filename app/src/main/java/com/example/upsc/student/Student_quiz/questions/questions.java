package com.example.upsc.student.Student_quiz.questions;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.example.upsc.R;
import com.example.upsc.common.AppController;
import com.example.upsc.common.Config;
import com.example.upsc.student.Student_quiz.quiz_menu.quizItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class questions extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private  ListView listView_questions;
    private List<questionitems>questionitems;
    private com.example.upsc.student.Student_quiz.questions.questionadapter questionadapter;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    public questions() {
        // Required empty public constructor
    }


    public static questions newInstance(String param1, String param2) {
        questions fragment = new questions();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_questions, container, false);
        listView_questions = (ListView) v.findViewById(R.id.student_quiz_questions);
        questionitems = new ArrayList<questionitems>();
        questionadapter = new questionadapter(getActivity(),questionitems,getContext());
        listView_questions.setAdapter(questionadapter);
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(Config.URL_ques_student);
        if(entry != null){
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");

                parseJsonFeed(data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }
        else {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading ...");
            progressDialog.show();
            StringRequest jsonReq = new StringRequest(Request.Method.GET,
                    Config.URL_ques_student,  new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
        return v;
    }

    private void parseJsonFeed(String response) {
        try {

            JSONArray questionArray = new JSONArray(response);
            Toast.makeText(getContext(),Integer.toString(questionArray.length()),Toast.LENGTH_LONG).show();

            for (int i = 0; i < questionArray.length(); i++) {
                JSONObject feedObj = (JSONObject) questionArray.get(i);


                questionitems item = new questionitems();
                  String question_type= feedObj.getString(Config.ques_type);
                  item.setQuestion_type(question_type);
                if(question_type.equals("1")) {
                    item.setQues_no(feedObj.getString("ques_no"));
                    //Toast.makeText(getContext(),feedObj.getString("ques_title"),Toast.LENGTH_LONG).show();

                    item.setQues_title(feedObj.getString("ques_title"));

                    item.setOption1(feedObj.getString("option1"));

                    item.setOption2(feedObj.getString("option2"));
                    item.setOption3(feedObj.getString("option3"));

                    item.setOption4(feedObj.getString("option4"));
                    Log.d("title", feedObj.getString("option1"));

                    item.setAns(feedObj.getString("ques_ans"));
                    item.setSolution(feedObj.getString("ques_solution"));
                    questionitems.add(item);
                }
                else {
                    item.setQues_title(feedObj.getString("ques_title"));
                    Toast.makeText(getContext(),feedObj.getString("ques_title"),Toast.LENGTH_LONG).show();
                    item.setShort_ans(feedObj.getString("short_answer"));
                    questionitems.add(item);
                }
            }

            // notify data changes to list adapater
            questionadapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
