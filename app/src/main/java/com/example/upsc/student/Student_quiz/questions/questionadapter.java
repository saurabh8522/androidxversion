package com.example.upsc.student.Student_quiz.questions;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.example.upsc.R;
import com.example.upsc.common.AppController;
import com.example.upsc.student.Student_quiz.quiz_menu.quizItems;

import java.util.List;

public class questionadapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;

    private List<com.example.upsc.student.Student_quiz.questions.questionitems> questionitems;
    private Context context;

    public questionadapter(Activity activity, List<com.example.upsc.student.Student_quiz.questions.questionitems> questionitems, Context context) {
        this.activity = activity;
        this.questionitems = questionitems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return questionitems.size();
    }

    @Override
    public Object getItem(int position){
        return questionitems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null)
            inflater = (LayoutInflater)activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = inflater.inflate(R.layout.questions_part,null);
         final LinearLayout linearLayout_full_layout = (LinearLayout)convertView.findViewById(R.id.linear_layout_full_question_layout);
        //Button submit_button = (Button)convertView.findViewById(R.id.check_answer_button);
        /*
        TextView ques_no = (TextView) convertView.findViewById(R.id.ques_number);
        TextView ques_title = (TextView) convertView.findViewById(R.id.ques_title);
        final RadioButton option1 = (RadioButton) convertView.findViewById(R.id.rid1);
        RadioButton option2 = (RadioButton) convertView.findViewById(R.id.rid2);
        RadioButton option3 = (RadioButton) convertView.findViewById(R.id.rid3);
        RadioButton option4 = (RadioButton) convertView.findViewById(R.id.rid4);
        final RadioGroup radioGroup_ans = (RadioGroup)convertView.findViewById(R.id.radio_group_options);
        //TextView ans = (TextView) convertView.findViewById(R.id.answer);
        TextView sol = (TextView) convertView.findViewById(R.id.solution);
        final LinearLayout linearLayout_sol = (LinearLayout)convertView.findViewById(R.id.sol_layout);
            */
        final questionitems item = questionitems.get(position);
        /*ques_no.setText(item.getQues_no());
        ques_title.setText(item.getQues_title());
        option1.setText(item.getOption1());
        option2.setText(item.getOption2());
        option3.setText(item.getOption3());
        option4.setText(item.getOption4());
        ans.setText(item.getAns());
        sol.setText(item.getSolution());*/
        if(item.getQuestion_type().equals("2")){
            TextView textView = new TextView(context);
            textView.setText(item.getQues_title());
            Toast.makeText(context,item.getQues_title(),Toast.LENGTH_LONG).show();
            EditText editText = new EditText(context);
            editText.setHint("Please write your answer here");

            linearLayout_full_layout.addView(textView);
            linearLayout_full_layout.addView(editText);

            Button button = new Button(context);
            button.setText("Submit");
            linearLayout_full_layout.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView textView_sol = new TextView(context);
                    textView_sol.setText(item.getShort_ans());
                    linearLayout_full_layout.addView(textView_sol);

                }
            });



        }
        else {
            TextView textView_question  = new TextView(context);
            textView_question.setText(item.getQues_title());
            linearLayout_full_layout.addView(textView_question);

            RadioGroup radioGroup = new RadioGroup(context);
            radioGroup.setOrientation(LinearLayout.VERTICAL);
            linearLayout_full_layout.addView(radioGroup);
            RadioButton option1 = new RadioButton(context);
            RadioButton option2 = new RadioButton(context);
            RadioButton option3 = new RadioButton(context);
            RadioButton option4 = new RadioButton(context);

            option1.setText(item.getOption1());
            option2.setText(item.getOption2());
            option3.setText(item.getOption3());
            option4.setText(item.getOption4());







        }
        /*
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout_sol.setVisibility(View.VISIBLE);
                int selectedid = radioGroup_ans.getCheckedRadioButtonId();
                selectedid = selectedid - option1.getId() +1;
                String selectedId = Integer.toString(selectedid);
                Toast.makeText(context,"SELECTED ID IS : AND ANS IS:"+ selectedId+item.getAns(),Toast.LENGTH_LONG).show();
                if(item.getAns().equals(selectedId))
                linearLayout_full_layout.setBackgroundColor(Color.GREEN);
                else
                    linearLayout_full_layout.setBackgroundColor(Color.RED);




            }
        });
        */
        //quiz_name.setText("abc");
        return convertView;
    }
}
