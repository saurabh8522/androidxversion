package com.example.upsc.common;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.database.*;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.upsc.R;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignUp.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignUp#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUp extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final  String DATABASE_NAME = "upsc";
    SQLiteDatabase mdatabase;
    private EditText profile_name, profile_address, profile_email, profile_pass, profile_re_pass,profile_phone, profile_channel;
    private Button signup;
    RadioGroup profile_role_chooser;
    String final_pass;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SignUp() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUp.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUp newInstance(String param1, String param2) {
        SignUp fragment = new SignUp();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
       // mdatabase = SQLiteDatabase.openOrCreateDatabase(DATABASE_NAME,null);
     //   mdatabase = SQLiteDatabase.openOrCreateDatabase(DATABASE_NAME,null);
      //  createStudentTable();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
             View view =    inflater.inflate(R.layout.fragment_sign_up, container, false);
             profile_name = (EditText)view.findViewById(R.id.input_name);
             profile_address = (EditText)view.findViewById(R.id.input_address);
             profile_email  = (EditText)view.findViewById(R.id.input_email);
             profile_phone  = (EditText)view.findViewById(R.id.input_mobile);
        profile_channel  = (EditText)view.findViewById(R.id.input_channel);
             profile_pass = (EditText)view.findViewById(R.id.input_password);
             signup = (Button)view.findViewById(R.id.btn_signup);
             signup.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     addstudent();
                 }
             });

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

   /*
    public void createStudentTable(){

        mdatabase.execSQL( "create table student if not exists student(\n" + "id INTEGER not null primary key AUTOINCREMENT,\n" +
                "name varchar(200) "+
                "address varchar(500)"+
                "email varchar(200)"+
                "phone varchar(200)" +
                "password varchar(200)" +

                ");"

        );

    }
    */
    public void addstudent(){
        final String string_name = profile_name.getText().toString().trim();
        final String  strng_address = profile_address.getText().toString();
        final String string_email = profile_email.getText().toString().trim();
        final String string_phone = profile_phone.getText().toString().trim();
        final String string_channel = profile_channel.getText().toString().trim();
        final String string_pass = profile_pass.getText().toString().trim();

        try {
              final_pass  = AESCrypt.encrypt(string_pass);
        }
        catch (Exception e){

                    }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.URL_Normal_Signup_Student,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //  SharedPrefsMethods.saveprofiledata("Logined",email,mobile,spinner,name);


                        Toast.makeText(getActivity(),ServerResponse,Toast.LENGTH_LONG).show();

                        // Hiding the progress dialog after all task complete.





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.


                        // Showing error message if something goes wrong.
                        Toast.makeText(getContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                // The first argument should be same sa your MySQL database table columns.
                params.put(Config.name,string_name );
                params.put(Config.mobile,string_phone);
                params.put(Config.address,strng_address);
                params.put(Config.email,string_email);
                params.put(Config.password,final_pass);
                params.put(Config.channel,string_channel);
                //  params.put("User_Password", PasswordHolder);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);


    }



}
