package com.example.upsc.student.Student_quiz.quiz_menu;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.upsc.R;
import com.example.upsc.common.AppController;

import java.util.List;

public class quizadapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;

    private List<com.example.upsc.student.Student_quiz.quiz_menu.quizItems> quizItems;
    private Context context;
    ImageLoader imageLoader  = AppController.getInstance().getImageLoader();
    public quizadapter(Activity activity, List<com.example.upsc.student.Student_quiz.quiz_menu.quizItems> quizItems, Context context) {

        this.activity = activity;
        this.quizItems = quizItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return quizItems.size();
    }

    @Override
    public Object getItem(int position){
        return quizItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null)
            inflater = (LayoutInflater)activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = inflater.inflate(R.layout.quizlist,null);

        if(imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView quiz_name = (TextView)convertView.findViewById(R.id.quiz_name);
        NetworkImageView quiz_image = (NetworkImageView)convertView.findViewById(R.id.quiz_image);
        LinearLayout linearLayout_quiz = (LinearLayout)convertView.findViewById(R.id.quiz_layout);
        final quizItems item = quizItems.get(position);
        quiz_name.setText(item.getQuiz_name());
        //quiz_name.setText("abc");
        quiz_image.setImageUrl(item.getQuiz_image(),imageLoader);


        return convertView;
    }
}
